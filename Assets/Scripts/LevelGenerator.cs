﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public enum TileType {
    Empty = 0,
    Player,
    Enemy,
    Wall,
    Door,
    Key,
    Dagger,
    End
}

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] tiles;

    public int minRoomSize, maxRoomSize;
    public int width = 64;
    public int height = 64;
    TileType[,] grid;

    protected void Start()
    {
        grid = new TileType[width, height];
        
        NodeDungeon rootNode = new NodeDungeon(new Rect(0, 0, width, height));
        CreateBsp(rootNode);
        rootNode.CreateRoom();

        //Creates wall with the width and height given
        FillBlock(grid, 0, 0, width, height, TileType.Wall);

        DrawRooms(rootNode);

        Debugger.instance.AddLabel(32, 26, "Room 1");

        //use 2d array (i.e. for using cellular automata)
        CreateTilesFromArray(grid);
    }

    //fill part of array with tiles
    private void FillBlock(TileType[,] grid, int x, int y, int width, int height, TileType fillType) {
        for (int tileY=0; tileY<height; tileY++) {
            for (int tileX=0; tileX<width; tileX++) {
                grid[tileY + y, tileX + x] = fillType;
            }
        }
    }

    //use array to create tiles
    private void CreateTilesFromArray(TileType[,] grid) {
        int height = grid.GetLength(0);
        int width = grid.GetLength(1);
        for (int y=0; y<height; y++) {
            for (int x=0; x<width; x++) {
                 TileType tile = grid[y, x];
                 if (tile != TileType.Empty) {
                     CreateTile(x, y, tile);
                 }
            }
        }
    }

    //create a single tile
    private GameObject CreateTile(int x, int y, TileType type) {
        int tileID = ((int)type) - 1;
        if (tileID >= 0 && tileID < tiles.Length)
        {
            GameObject tilePrefab = tiles[tileID];
            if (tilePrefab != null) {
                GameObject newTile = GameObject.Instantiate(tilePrefab, new Vector3(x, y, 0), Quaternion.identity);
                newTile.transform.SetParent(transform);
                return newTile;
            }

        } else {
            Debug.LogError("Invalid tile type selected");
        }

        return null;
    }

    public void CreateBsp(NodeDungeon node) {
        if (node.IsLeaf()) {
            if(node.room.width > maxRoomSize || node.room.height > maxRoomSize || Random.Range(0.0f, 1.0f) > 0.25f) {
                if(node.Split(minRoomSize, maxRoomSize)) {
                    CreateBsp(node.left);
                    CreateBsp(node.right);
                }
            }
        }
    }

    public void DrawRooms(NodeDungeon node) {
        if (node == null)
            return;

        if (node.IsLeaf()) {
            for (int x = (int)node.room.x; x < node.room.xMax; x++) {
                for (int y = 0; y < node.room.yMax; y++) {
                    grid[x, y] = TileType.Empty;
                }
            }
        }else {
            DrawRooms(node.left);
            DrawRooms(node.right);
        }
    }

    public class NodeDungeon {

        public NodeDungeon left, right;
        public Rect room = new Rect(-1, -1, 0, 0);

        public NodeDungeon(Rect room) {
            this.room = room;
        }

        public bool IsLeaf() {
            return left == null && right == null;
        }

        public bool Split(int minRoomSize, int maxRoomSize) {
            if (!IsLeaf()) {
                return false;
            }

            bool splitHorizontally;
            if((room.width / room.height) > 1.25f) {
                splitHorizontally = false;
            } else if((room.height / room.width) > 1.25f) {
                splitHorizontally = true;
            } else {
                splitHorizontally = Random.Range(0.0f, 1.0f) > 0.5f;
            }

            if(Mathf.Min(room.width, room.height) / 2 < minRoomSize) {
                return false;
            }

            if (splitHorizontally) {
                int split = Random.Range(minRoomSize, (int)room.width - minRoomSize);

                left = new NodeDungeon(new Rect(room.x, room.y, room.width, split));
                right = new NodeDungeon(new Rect(room.x + split, room.y, room.width - split, room.height));
            } else {
                int split = Random.Range(minRoomSize, (int)room.height - minRoomSize);

                left = new NodeDungeon(new Rect(room.x, room.y, split, room.height));
                right = new NodeDungeon(new Rect(room.x, room.y + split, room.width, room.height - split));
            }

            return true;
        }

        public void CreateRoom() {
            if(left != null) {
                left.CreateRoom();
            }

            if(right != null) {
                right.CreateRoom();
            }

            if (IsLeaf()) {
                int roomWidth = (int)Random.Range(room.width / 2, room.width - 2);
                int roomHeight = (int)Random.Range(room.height / 2, room.height - 2);
                int roomX = (int)Random.Range(1, room.width - roomWidth - 1);
                int roomY = (int)Random.Range(1, room.height - roomHeight - 1);

                room = new Rect(room.x + roomX, room.y + roomY, roomWidth, roomHeight);
            }
        }
    }
}
